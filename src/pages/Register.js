import React, { useContext, useState, Component } from "react";
import axios from "axios";
import { UserContext } from "../context/UserContext";
import {Link as LinkRouter,useHistory,Redirect } from "react-router-dom"
import {
  Grid,
  Container,
  FormGroup,
  FormControl,
  TextField,
  Button,
} from "@material-ui/core";

const Register = () => {
  const [apiUser,,setUser,inputUser,setInputUser] = useContext(UserContext);
    const [redirect,setRedirect] = useState(false)
    const [messages,setMessages] = useState("");
    const history = useHistory()
   

  const handleSubmit = (event) => {
    if ((inputUser.username.replace(/\s/g,'') === '') || (inputUser.password.replace(/\s/g,'') === '')) {
            setMessages("Please Fill Username and Password")
            return
        }

        
            axios.post(apiUser,{
                created_at : new Date(),
                username : inputUser.username,
                password : inputUser.password
            })
            .then(res => {
                if (res.data.id) {
                  setUser(null)
                  setRedirect(true)
                  history.goBack()
                }else{
                  setRedirect(false)
                  setMessages(res.data)
                }
                
            }).catch(error => {
                console.log(error)
                setMessages(error.messages)
            })
        
            setInputUser({
              username : "",
              password : ""
            })
            setMessages("")
    }

  const handleChange = (event) => {
     let typeOfInput = event.target.name
        
        switch (typeOfInput) {
          case "username":
            setInputUser({...inputUser,username: event.target.value});
            break;
          case "password":
            setInputUser({...inputUser,password: event.target.value});
            break;
          default:
            break;
        }
      }

  return (
    <>
      <div className="login-page">
        <Container fixed style={{ margin: "50px" }}>
          <Grid container spacing={3}>
            <Grid item md={7} className="judul">
              <h1>Finale</h1>
              
            </Grid>
            <Grid item md={1}></Grid>
            <Grid item md={4}>
              <Grid className="login-box">
                <h3>Buat Akun Kamu</h3>
                <p className="h3-padding">Silahkan isi formulir dibawah.</p>
                <form onSubmit={handleSubmit}>
                  <FormGroup>
                    <FormControl>
                      <TextField
                        onChange={handleChange}
                        value={inputUser.username}
                        name="username"
                        id="username"
                        className="form-fill"
                        label="Username"
                        autoComplete="username"
                        variant="outlined"
                        fullWidth
                        required
                      />
                      <TextField
                        id="password"
                        className="form-fill"
                        label="Password"
                        variant="outlined"
                        fullWidth
                        type="password"
                        required
                        name="password"
                        autoComplete="current-password"
                        onChange={handleChange}
                        value={inputUser.password}
                      />
                      <Button
                        onClick={handleSubmit}
                        className="button btn-daftar"
                        variant="contained"
                        color="primary"
                        fullWidth
                      >
                        Daftar
                      </Button>
                    </FormControl>
                    <p className="silahkan-masuk">
                      Sudah punya Akun? Silahkan <a href="/login">Masuk</a>
                    </p>
                  </FormGroup>
                </form>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </div>
    </>
  );
};

export default Register;

import React, { useContext, useState } from "react";
import {UserContext} from "../context/UserContext";
import axios from "axios";
import { useHistory } from "react-router-dom";

const Login = () => {
    const [user, setUser] = useContext(UserContext);
    const history = useHistory();
    const [redirect,setRedirect] = useState(false)
    const [input, setInput] = useState({
      username: "",
      password: "",
      createUsername: "",
      createPassword: "",
    });
  
    const handleSubmit = (event) => {
      event.preventDefault();
      if ((input.username.replace(/\s/g,'') === '') || (input.password.replace(/\s/g,'')=== '')) {
          alert("Please Input Username or Password")
          return
        }
      axios.post(`https://backendexample.sanbersy.com/api/login`, {
          username: input.username,
          password: input.password,
        })
        .then(res => {
            setUser({
              username: res.data.username,
              password: res.data.password,
            });
            if (res.data.id) {
              
            localStorage.setItem("user",
              JSON.stringify({
                id: res.data.id,
                username: res.data.username,
                password: res.data.password,
              })
            );
            
            alert("Login Successful!");
            setRedirect(true)
            
          } else {
            setRedirect(false)
           
          }
        });
      setInput({ username: "", password: "" });
      alert("Login Successfull!");
    };
  
    const handleCreate = (event) => {
      event.preventDefault();
      let date = new Date().toLocaleString();
      axios.post(`https://backendexample.sanbersy.com/api/users`, {
        created_at: date,
        username: input.createUsername,
        password: input.createPassword,
      });
      setInput({ ...input, createUsername: "", createPassword: "" });
      alert("Account created!");
    };
  
    const handleChange = (event) => {
      let value = event.target.value;
      let name = event.target.name;
      switch (name) {
        case "username": {
          setInput({ ...input, username: value });
          break;
        }
        case "password": {
          setInput({ ...input, password: value });
          break;
        }
        case "createUsername": {
          setInput({ ...input, createUsername: value });
          break;
        }
        case "createPassword": {
          setInput({ ...input, createPassword: value });
          break;
        }
        default: {
          break;
        }
      }
    };
  
    return (
      <>
        <div className="login-page">
        
          <form 
            className="loginForm" 
            onSubmit={handleSubmit} 
            >
          <div class="login-box">
          <h3 className="h3-padding">Login</h3>
          <p className="silahkan-masuk">
                      Sudah punya Akun? Silahkan isi form dibawah ini
                    </p>
            <label >Username</label>
            <input class="form-fill" type="text" name="username" value={input.username} onChange={handleChange}/>
          <br></br>
            <label for="exampleInputPassword1">Password</label>
            <input class="form-fill" type="password" name="password" value={input.password} onChange={handleChange}/>
          <br></br>
          <button type="submit" class="btn btn-primary">Masuk</button>
          </div>
          </form>

          <form
            className="loginForm"
            onSubmit={handleCreate}
          >
          <div class="login-box">
          <h3 className="h3-padding">Register</h3>
          <p className="silahkan-masuk">
                      Belum punya Akun? Silahkan isi form dibawah ini
                    </p>
            <label>new Username: </label>
            <input class="form-fill" type="text" name="createUsername" value={input.createUsername} onChange={handleChange}/>
            <br></br>
            <label>new Password: </label>
            <input class="form-fill"  type="text" name="createPassword" value={input.createPassword} onChange={handleChange}/>
            <br></br>
            <button type="submit" className="btn btn-primary">Submit</button>
            </div>
          </form>
          
        </div>
      </>
    );
  };
  
  export default Login;